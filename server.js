const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const cors = require('cors')
const axios = require('axios');
require('dotenv').config()
const port = process.env.port

var urlOrder = process.env.urlOrder 
var urlQR = process.env.urlQR
var access_token = process.env.access_token 

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
 extended: true
}))

const auth = (req, res, next) => {

  try {
  let token = req.headers['x-access-token'] || req.headers['authorization'];
  console.log(token)
  if (token.startsWith('Bearer ')) {
    token = token.slice(7, token.length);
  }
  if (!token) {
    return res.ststus(400).json({ error: true, msg: 'Token is not provided.' })
  } 

  } catch (err) {
    console.log(err)
    return res.status(400).json({ error: true, msg: err})
  }
}

app.get('/', (req, res) => {
  res.send('Kbank QR-Payment')
})

// create orderid from Kbank
app.post('/api/create/order', (req, res) => {
  let data = req.body
  //console.log(data)
  
  try {
    if (!data) {
      return res.status(400).json({error: true, msg: "missing id."})
    }
    const headers = {
      'Content-Type': 'application/json',
      'x-api-key': access_token
    }

    axios.post(urlOrder, data, {
      headers: headers
    })
    .then((response) => {
      //console.log(response.data)
      return res.status(200).json({ error: false, data: response.data})
    })
    .catch((err) => {
      console.log(err) 
      return res.status(400).json({ error: true, data: err})
    })

  } catch (err) {
     res.status(200).json({ 
       error: true,
       msg: "get order id fail", 
       data: [] 
   })}
})

// Inquiry Order
app.post('/api/inquiry/order', (req, res) => {
  let data = req.body.orderId

  try {
    if (!req.body.orderId) {
      return res.status(400).json({error: true, msg: "missing id."})
    }
    const headers = {
      'Content-Type': 'application/json',
      'x-api-key': access_token
    }

    axios.get(urlOrder+data, {
      headers: headers
    })
    .then((response) => {
      //console.log(response.data)
      return res.status(200).json({ error: false, data: response.data})
    })
    .catch((err) => {
      console.log(err) 
      return res.status(400).json({ error: true, data: err})
    })

  } catch (err) {
     res.status(200).json({ 
       error: true,
       msg: "get order id fail", 
       data: [] 
   })}
})

// Inquiry QR (send order id)
app.post('/api/inquiry/qr', (req, res) => {
  let data = req.body.orderId

  try {
    if (!req.body.orderId) {
      return res.status(400).json({error: true, msg: "missing order id."})
    }
    const headers = {
      'x-api-key': access_token
    }

    axios.get(urlQR+data, {
      headers: headers
    })
    .then((response) => {
      //console.log(response.data)
      return res.status(200).json({ error: false, data: response.data})
    })
    .catch((err) => {
      //console.log(err) 
      if(err.response.status == 417) {
        //console.log(err.response.data)
        return res.status(200).json({ error: false, data: err.response.data })
      }
      return res.status(400).json({ error: true, data: err})
    })

  } catch (err) {
     res.status(200).json({ 
       error: true,
       msg: "get qr id fail", 
       data: [] 
   })}
})

// Cancel QR Transaction
app.post('/api/cancel/qr', (req, res) => {
  let data = req.body.qrId

  try {
    if (!req.body.qrId) {
      return res.status(400).json({error: true, msg: "missing QR id."})
    }
    const headers = {
      'Content-Type': 'application/json',
      'x-api-key': access_token
    }

    axios.get(urlOrder+data+'/cancel', {
      headers: headers
    })
    .then((response) => {
      //console.log(response.data)
      return res.status(200).json({ error: false, data: response.data})
    })
    .catch((err) => {
      console.log(err) 
      return res.status(400).json({ error: true, data: err})
    })

  } catch (err) {
     res.status(200).json({ 
       error: true,
       msg: "cancel qr id fail", 
       data: [] 
   })}
})

app.listen(port, () => {
  console.log(`Start server at port ${port}.`)
})
