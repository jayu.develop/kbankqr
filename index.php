<?php
  echo "register";
?>

<script>
  console.log('register');
  async function kbankQR(url = '', data = {}) {
    const response = await fetch(url, {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin' ,
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer', 
      body: JSON.stringify(data)
    });
    return response.json();
  }

  let serverIp = "localhost";
  //let serverIp = "161.200.145.2";
  let urlOrder = "http://"+serverIp+":4000/api/create/order";
  let urlQR = "http://"+serverIp+":4000/api/inquiry/qr";

  let dataCreateOrder = {
    "amount": 10.00,
    "currency": "THB",
    "description": "VISITOR",
    "source_type": "qr",
    "reference_order": "TRO0000030"
  };

  let dataInquiryQR = {
    "orderId": "order_test_20496c15dae3c85e346d082fea1b4daf0cd1e"
  };

  let createOrderId = kbankQR(urlOrder, dataCreateOrder)
  .then(res => {
    let orderId = res.data.id;
    console.log(res.data);
    console.log(orderId);
  });

  let inquiryQRcode = kbankQR(urlQR, dataInquiryQR)
  .then(res => {
    console.log(res.data);
  });

</script>
